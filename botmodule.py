import discord
from discord.ext import commands
######################
from inspect import cleandoc
from typing import Union
import os
import requests
from pyupload.uploader import *
import cv2
import shutil
import subprocess
#######################
import torch
import modules.generate as generate
import modules.input_and_output as input_and_output
#######################
from random import seed
from random import random
#######################
import platform
from typing import Awaitable
import time

async def interpolatevideo(ctx, arg1="--model",arg2="HubertHD-RL", arg3="--discord", arg4="x"):
    wrong=False
    ytdl=False
    model_name="HubertHD-RL"
    gifuse=False
    wgeturl="fdf"
    ytdlurl="none"
    wget=False
    ################  ################
    if arg1=="--model":
        model_name=arg2
    if arg3=="--model":
        model_name=arg4
    ################
    if arg1=="--ytdl":
        ytdlurl=arg2
        ytdl=True
    if arg3=="--ytdl":
        ytdlurl=arg4
        ytdl=True
    ##############
    if arg1=="--wget":
        wgeturl=arg2
        wget=True
    if arg3=="--wget":
        wgeturl=arg4
        wget=True

    embedVar = discord.Embed(title="Settings", description="", color=0x4287f5)
    embedVar.add_field(name="Model", value=model_name, inline=False)
    embedVar.add_field(name="youtube-dl", value=ytdl, inline=False)
    embedVar.add_field(name="youtube-dl url", value=ytdlurl, inline=False)
    for char in ytdlurl:
        if char==";":
            
            wrong=True
    if wrong==True:
        ytdlurl="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
    for char in wgeturl:
        if char==";":
            
            wrong=True
    if wrong==True:
        wgeturl="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
    await ctx.channel.send(embed=embedVar)
    filename=int(random()*100000000000000)
    try:
        os.mkdir(f"{filename}")
    except:
        _="c"
    ################ Downloading video ################



    message =  await ctx.send(content=f"Downloading video 📥\n")

    if ytdl==True:
        print("using youtube-dl")
        os.system(f"youtube-dl -o {filename} --merge-output-format mkv {ytdlurl}")
    elif wget==True:
        print("using wget")
        os.system(f"curl {wgeturl} -o {filename}.mkv ")
        if wgeturl[-1]=="f":
            gifuse=True
        else:
            gifuse=False
    else:
        attachment = ctx.message.attachments[0] # gets first attachment that user
        os.system(f"wget --output-document={filename}.mkv {attachment.url}")
        length = len(attachment.url)
        if attachment.url[length -1]=="f":
            gifuse=True
        else:
            gifuse=False


    ################ Using cv2 to get fps, width, height, frames number ################


    try:
        video = cv2.VideoCapture(f"{filename}.mkv");
        fps = video.get(cv2.CAP_PROP_FPS)
        width  = video.get(cv2.CAP_PROP_FRAME_WIDTH)   # float `width`
        height = video.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float `height` round(width)
        frames = video.get(cv2.CAP_PROP_FRAME_COUNT)
        length = int(frames/fps)+2
        bitrate = int(800000/length)
        await message.edit(content=f"bitrate {bitrate}K/s, fps: {fps*2}, frames: {frames}\n")
    except Exception as e:
        await  ctx.channel.send(content=f"error❌\n\n{e}")
    aspect_ratio=width/height
    if height>721 and width>1281 or width>721 and height>1281:
        if width<height:
            width=720
            height=1280
        else:
            height=720
            width=1280

    ################ Using ffmpeg to extract frames and audio ################
    os.system(f"ffmpeg -i {filename}.mkv -hide_banner -loglevel error -vf scale={int(width/8)*8}:{int(height/8)*8}:flags=bicubic -pix_fmt rgb24 -q 1 {filename}/%6d.png")
    os.system(f"ffmpeg -i {filename}.mkv  -hide_banner -loglevel error -vn -c:a copy {filename}-audio.mkv")
    if fps==0.0:
        fps=25
    await message.edit(content="finished extracting🎞️\n")


    ################ using interpolation ################


    await message.edit(content="interpolating✨\n")
    try:
        generate.interpolation(batch_size=2, img_fmt="png", temp_img = f"{filename}", modelp=f"models/{model_name}.pth")
    except Exception as e:
        await  ctx.channel.send(content=f"interpolation crashed❌\n{e}")
    await message.edit(content="finished interpolation✨\n")


    ################ Using ffmpeg to encode video ################
    time.sleep(5)
    input_and_output.rename(f"./{filename}",filetype=".png")


    await message.edit(content=f"encoding🗄️➡️🎞️\n")


    ################ gif encoidng ################

    if gifuse==True:
        os.system(f'ffmpeg -r {fps*2} -i "{filename}/%6d.png" -filter_complex "scale={int(width)}:{int(height)}:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -hide_banner -loglevel error -fs 7.95M "{filename}.gif"')
        filegif=discord.File(f"{filename}.gif")

    ################ mp4 encoidng ################-b:v {bitrate-69}k  -rc 1
    
    if gifuse==True:
        await ctx.channel.send(file=filegif,content=f"finished gif encoding\n")
    

    if os.path.isfile(f'{filename}-audio.mkv'):
        os.system(f'ffmpeg -r {fps*2} -i "{filename}/%6d.png" -i {filename}-audio.mkv -pix_fmt yuv420p -b:v {bitrate/0.7}k -c:v h264_nvenc -preset p7 -b:a 256k -qp 24 "{filename}_highbitrate.mp4"')
    else:
        os.system(f'ffmpeg -r {fps*2} -i "{filename}/%6d.png" -pix_fmt yuv420p -b:v {bitrate/0.7}k -c:v h264_nvenc -preset p7 -b:a 256k -qp 24 "{filename}_highbitrate.mp4"')     
    
    uploader_class =  FileioUploader
    uploader_instance = uploader_class(f"{filename}_highbitrate.mp4")
    catboxurl = uploader_instance.execute()
    await message.edit( content=f"finished! {catboxurl}")
    #delete video
    try:
        os.remove(f"{filename}_highbitrate.mp4")
    except:
        pass
    try:
        os.remove(f"{filename}.webm")
    except:
        pass
    try:
        os.remove(f"{filename}.gif")
    except:
        pass
    try:
        os.remove(f"{filename}.mkv")
    except:
        pass
    try:
        os.remove(f"{filename}-audio.mkv")
    except:
        pass
    shutil.rmtree(f"{filename}/")
    torch.cuda.empty_cache()
