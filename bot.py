import discord
from discord.ext import commands
######################
from inspect import cleandoc
from typing import Union
import os
import requests
import cv2
import shutil
import glob
import subprocess
#######################
import torch
import modules.generate as generate
import modules.input_and_output as input_and_output
#######################
from random import seed
from random import random
import random
#######################
import platform
ossystem=platform.system()
print(ossystem)
from botmodule import *
import threading
import asyncio
import argparse

#######################
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--token',type=str)
args, unparsed = parser.parse_known_args()
token=args.token
#######################

activity=discord.Streaming(name="Escaping from hubert", url="")

bot = commands.Bot(command_prefix='!',activity=activity,status=discord.Status.online)

#def commands
os.system("nvidia-smi")
@bot.command()
async def gpu(ctx):
    await ctx.channel.send(content=f"bot is running on {torch.cuda.get_device_name(0)}\nvram: {int(torch.cuda.get_device_properties('cuda').total_memory/1024/1024/1024)}GB\nos: {ossystem}\npytorch: {torch.__version__}\nonnx verssion: ")
    


@bot.command()
async def interpolate(ctx, arg1="--model",arg2="HubertHD-RL", arg3="--discord", arg4="x"):
    '''interpolate your video\n 
       !interpolate --ytdl [URL] 
       !interpolate --wget [URL]
       !interpolate [ADD ATTACHMENT] 
        select model by adding --model (name)'''
    asyncio.gather(interpolatevideo(ctx, arg1=arg1,arg2=arg2, arg3=arg3, arg4=arg4))

@bot.command()
async def ping(ctx):
    await ctx.send(f'Pong! {int(bot.latency*100)}ms')
#
@bot.command()
async def status(ctx):
    await ctx.send('bot work. now..')

#

#######################



    #
#
@bot.command()
async def credits(ctx):
    await ctx.channel.send(content="The bot was created by Hubert Sontowski")
#
@bot.command()

async def models(ctx):
    '''Lists current models'''
    modlist="\n"
    for file in glob.glob("models/*.pth"):
        file=file[7:]
        modlist+=file+"\n"
    embed = discord.Embed(
        title="List of current models",
        color=0x4287f5,
        description=cleandoc(f"{modlist}")
    )
    await ctx.send(embed=embed)
                           

bot.run(token)

